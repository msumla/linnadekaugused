package systeem;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class OtsiHinda {

	/*
	 * meetod, mis loeb veebilehelt html-koodi ridasi ja peatub, kui leiab
	 * m22ratud fraasi, millest eemaldatakse arvud ja see tagastatakse massiivina
	 */
	public static String[] main(String[] veeb) {

		String rida = null;
		String hind[] = new String[2];
		try {
			URL aadress = new URL("http://www.tankla.info/");
			InputStream is = aadress.openStream();
			BufferedReader loeRida = new BufferedReader(new InputStreamReader(is));

			// otsitakse ja k2rbitakse vajalik v22rtus
			while ((rida = loeRida.readLine()) != null) {
				if (rida.contains("<b>95</b>")) {
					hind[0] = rida.replaceAll("\\s", "").substring(13, 18);
				} else if (rida.contains("<b>D</b>")) {
					hind[1] = rida.replaceAll("\\s", "").substring(12, 17);
					break;
				} // if
			} // while
			loeRida.close();
			is.close();

		} catch (Exception e) {
			System.out.println("(!) Ilmnes viga");
			e.printStackTrace();
		}
		return hind;
	} // main meetod
} // OtsiHinda klass